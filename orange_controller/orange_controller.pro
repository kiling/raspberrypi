#-------------------------------------------------
#
# Project created by QtCreator 2016-03-03T00:17:44
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = orange_controller
CONFIG   += console
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -std=c++0x

TEMPLATE = app

INCLUDEPATH += $[QT_SYSROOT]/usr/local/include
LIBS += -L$[QT_SYSROOT]/usr/local/lib -lwiringPi

SOURCES += main.cpp \
    controller.cpp \
    ../pisocket.cpp

HEADERS += \
    controller.h \
    ../pisocket.h
