/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>
#include "joystick.h"

Joystick::Joystick(int keyMap, QWidget *parent): QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);//for optimizing painting events

    m_keyMap = keyMap;
    m_drawRadius = 120;
    m_drawSmailRaduis = 20;
    m_padding = 6;
    m_radius = (m_drawRadius - m_drawSmailRaduis) / 2;

    resize(m_drawRadius + m_padding, m_drawRadius + m_padding);

    m_timer = new QTimer(this);
    QObject::connect(m_timer, &QTimer::timeout, this, &Joystick::updateTimer);
    m_timer->start(100);

    init();
}

void Joystick::setAxis(double x, double y)
{
    m_xAxis = x;
    m_yAxis = y;

    emit changeX(x);
    emit changeY(y);

    update();
}

void Joystick::onKeyPress(QKeyEvent *event)
{
    if(event->isAutoRepeat() )
        event->ignore();
    else
    {
        if((m_keyMap == 0 && event->key() == Qt::Key_Down) || (m_keyMap == 1 && event->key() == Qt::Key_S))
        {
            m_timeDown = 0;
            m_presDown = true;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Up) || (m_keyMap == 1 && event->key() == Qt::Key_W))
        {
            m_timeUp = 0;
            m_pressUp = true;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Left) || (m_keyMap == 1 && event->key() == Qt::Key_A))
        {
            m_timeLeft = 0;
            m_pressLeft = true;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Right) || (m_keyMap == 1 && event->key() == Qt::Key_D))
        {
            m_timeRight = 0;
            m_pressRigth = true;
        }

        event->accept();
    }
}

void Joystick::onKeyRelease(QKeyEvent *event)
{
    if(event->isAutoRepeat() )
        event->ignore();
    else
    {
        if((m_keyMap == 0 && event->key() == Qt::Key_Down) || (m_keyMap == 1 && event->key() == Qt::Key_S))
        {
            m_timeDown = 0;
            m_presDown = false;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Up) || (m_keyMap == 1 && event->key() == Qt::Key_W))
        {
            m_timeUp = 0;
            m_pressUp = false;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Left) || (m_keyMap == 1 && event->key() == Qt::Key_A))
        {
            m_timeLeft = 0;
            m_pressLeft = false;
        }

        if((m_keyMap == 0 && event->key() == Qt::Key_Right) || (m_keyMap == 1 && event->key() == Qt::Key_D))
        {
            m_timeRight = 0;
            m_pressRigth = false;
        }


        if (!m_presDown && !m_pressLeft && !m_pressRigth && !m_pressUp)
            init();

        event->accept();
    }
}

void Joystick::updateTimer()
{
    if (m_presDown)
        m_timeDown += 1;

    if (m_pressLeft)
        m_timeLeft += 1;

    if (m_pressRigth)
        m_timeRight += 1;

    if (m_pressUp)
        m_timeUp += 1;

    double speed = 0.05;

    double x = speed *( m_timeRight - m_timeLeft);
    double y = speed *( m_timeUp - m_timeDown);
    if (x > 1.0) x = 1.0;
    if (x < -1.0) x = -1.0;

    if (y > 1.0) y = 1.0;
    if (y < -1.0) y = -1.0;

    setAxis(x, y);
}

void Joystick::init()
{
    m_xAxis = 0.0;
    m_yAxis = 0.0;

    m_timeDown = 0;
    m_timeLeft = 0;
    m_timeRight = 0;
    m_timeUp = 0;

    m_presDown = false;
    m_pressLeft = false;
    m_pressRigth = false;
    m_pressUp = false;
}

void Joystick::paintEvent(QPaintEvent *)
{
    resize(m_drawRadius + m_padding, m_drawRadius + m_padding);

    QPainter painter(this);

    int w = width();
    int h = height();

    painter.drawRect(m_padding / 2, m_padding / 2, m_drawRadius, m_drawRadius);

    int x1 = m_padding / 2 + m_drawRadius / 2;
    int y1 = m_padding / 2;
    int y2 = m_padding / 2 + m_drawRadius;

    painter.drawLine(x1, y1, x1, y2);

    x1 = m_padding / 2;
    int x2 = m_padding / 2 + m_drawRadius;

    y1 = m_padding / 2 + m_drawRadius / 2;

    painter.drawLine(x1, y1, x2, y1);

    painter.setPen(Qt::DashLine);
    QColor hourColor(127, 0, 127);
    painter.setBrush(hourColor);

    int dX = m_xAxis * m_radius;
    int dY = -m_yAxis * m_radius;

    painter.drawEllipse((w - m_drawSmailRaduis)/ 2 + dX, (h - m_drawSmailRaduis) / 2 + dY, m_drawSmailRaduis, m_drawSmailRaduis);
}
