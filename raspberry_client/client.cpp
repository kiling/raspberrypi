#include "client.h"
#include <QDebug>

Client::Client(QObject *parent) :QObject(parent)
{
    m_raspberry = new PiSocket(parent);
    QObject::connect(m_raspberry, SIGNAL(commandReceived(Command&)),
                     this, SLOT(commandReceived(Command&)));

    m_raspberry->sendCommand("Greeting", "Hello raspberry pi! I'm client!");
}

bool Client::connectToRasbperry(QString ip, int port)
{
    return m_raspberry->connectToServer(ip, port);
}

void Client::sendCommand(Command &command)
{
    m_raspberry->sendCommand(command);
}

void Client::sendCommand(QString name, QString text)
{
    m_raspberry->sendCommand(name, text);
}

void Client::sendCommand(QString name, int value)
{
    m_raspberry->sendCommand(name, value);
}

void Client::commandReceived(Command &command)
{
    qDebug() << command.first << " " << command.second;
    emit getCommand(command);
}
