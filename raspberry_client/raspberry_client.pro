#-------------------------------------------------
#
# Project created by QtCreator 2015-08-16T15:03:48
#
#-------------------------------------------------

QT       += core gui network webkitwidgets multimediawidgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = raspberry_client
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    client.cpp \
    ../pisocket.cpp \
    joystick.cpp

HEADERS  += mainwindow.h \
    client.h \
    ../pisocket.h \
    joystick.h

FORMS    += mainwindow.ui
