#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include "../pisocket.h"

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = 0);
    bool connectToRasbperry(QString ip, int port);
    void sendCommand(Command &command);
    void sendCommand(QString name, QString text);
    void sendCommand(QString name, int value);
signals:
    void getCommand(Command &command);
private slots:
    void commandReceived(Command &command);
private:
    PiSocket *m_raspberry;
};

#endif // CLIENT_H
