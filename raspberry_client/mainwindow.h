#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "client.h"
#include "joystick.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void getCommand(Command &command);
    void on_btConnect_clicked();
    void disconnected();
    void changeCameraX(double x);
    void changeCameraY(double y);
    void changeMoveX(double x);
    void changeMoveY(double y);
private:
    void keyPressEvent(QKeyEvent *) override;
    void keyReleaseEvent(QKeyEvent *)override;
    void updateStickValue();
    void updateMoveValue();

    Ui::MainWindow *ui;
    Client *m_client;
    int m_cameraX, m_cameraY;
    int m_moveX, m_moveY;

    Joystick *m_cameraStick;
    Joystick *m_moveStick;
    QLabel *m_hint;
};

#endif // MAINWINDOW_H
