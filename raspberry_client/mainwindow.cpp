#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QVideoWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QWidget::grabKeyboard();
    ui->setupUi(this);

    m_client = new Client(this);
    QObject::connect(m_client, &Client::getCommand, this, &MainWindow::getCommand);

    m_cameraX = 90;
    m_cameraY = 90;
    m_moveX = 0;
    m_moveY = 0;

    m_client->sendCommand("servoX", m_cameraX);
    m_client->sendCommand("servoY", m_cameraY);
    m_client->sendCommand("stop", 0);

    m_cameraStick = new Joystick(0, this);
    QObject::connect(m_cameraStick, &Joystick::changeX, this, &MainWindow::changeCameraX);
    QObject::connect(m_cameraStick, &Joystick::changeY, this, &MainWindow::changeCameraY);
    ui->gbRigth->layout()->addWidget(m_cameraStick);

    m_moveStick = new Joystick(1, this);
    QObject::connect(m_moveStick, &Joystick::changeX, this, &MainWindow::changeMoveX);
    QObject::connect(m_moveStick, &Joystick::changeY, this, &MainWindow::changeMoveY);
    ui->gbLeft->layout()->addWidget(m_moveStick);

    m_hint = new QLabel(this);
    statusBar()->addWidget(m_hint);

    m_hint->setText("No connection");

    //**
    /*QMediaPlaylist *playlist = new QMediaPlaylist(this);
    playlist->addMedia(QUrl("file:///D:/1 серия.avi"));
    playlist->setCurrentIndex(1);

    QMediaPlayer *player = new QMediaPlayer(this);
    player->setPlaylist(playlist);

    QVideoWidget *videoWidget = new QVideoWidget(this);
    player->setVideoOutput(videoWidget);
    videoWidget->show();

    player->play();

    ui->verticalLayout_4->addWidget(videoWidget);*/
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getCommand(Command &)
{

}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    m_cameraStick->onKeyPress(event);
    m_moveStick->onKeyPress(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    m_cameraStick->onKeyRelease(event);
    m_moveStick->onKeyRelease(event);
}

void MainWindow::updateStickValue()
{
    if (m_cameraX > 180) m_cameraX = 180;
    if (m_cameraX < 0) m_cameraX = 0;

    if (m_cameraY > 180) m_cameraY = 180;
    if (m_cameraY < 0) m_cameraY = 0;
}

void MainWindow::updateMoveValue()
{
    if (m_moveY > 255) m_moveY = 255;
    if (m_moveY < 0) m_moveY = 0;
}

void MainWindow::on_btConnect_clicked()
{
    QString ip = ui->edIpAddress->text();
    int port = ui->sbPort->value();


    m_hint->setText("Сonnection...");
    if (m_client->connectToRasbperry(ip, port))
    {
        m_hint->setText("Connected successfully!");
        ui->btConnect->setEnabled(false);
    }
    else
    {
        m_hint->setText("Сonnection is not established!");
    }
}

void MainWindow::disconnected()
{
    m_hint->setText("No connection");
    ui->btConnect->setEnabled(true);
}

void MainWindow::changeCameraX(double x)
{
    double newX = 0;
    double dx = 75 * x * x;
    if (x > 0)
        newX = m_cameraX - dx;
    else
        newX = m_cameraX + dx;

    if (newX > 180) newX = 180;
    if (newX < 0) newX = 0;

    if (abs(newX - m_cameraX) > 0.001)
    {
        m_cameraX = newX;
        updateStickValue();

        m_client->sendCommand("servoX", m_cameraX);

        qDebug() << "Camera X: " << m_cameraX;
    }
}

void MainWindow::changeCameraY(double y)
{
    double newY = 0;
    double dy = 75 * y * y;
    if (y > 0)
        newY = m_cameraY - dy;
    else
        newY = m_cameraY + dy;

    if (newY > 180) newY = 180;
    if (newY < 0) newY = 0;

    if (abs(newY - m_cameraY) > 0.001)
    {
        m_cameraY = newY;
        updateStickValue();
        m_client->sendCommand("servoY", m_cameraY);
        qDebug() << "Camera Y: "<< m_cameraY;
    }
}

void MainWindow::changeMoveX(double x)
{

}

void MainWindow::changeMoveY(double y)
{
    int newY = abs((int)round(255.0 *  y));
    //qDebug() << "newY: "<< newY;
    if (newY != m_moveY)
    {
        m_moveY = newY;
        //updateStickValue();

        if (m_moveY < 10)
        {
            m_client->sendCommand("stop", 0);
            qDebug() << "Stop: "<< m_moveY;
        }
        else
        if (y > 0)
        {
            m_client->sendCommand("forward", m_moveY);
            qDebug() << "Forward: "<< m_moveY;
        }
        else
        if (y < 0)
        {
             m_client->sendCommand("backward", m_moveY);
             qDebug() << "Backward: "<< m_moveY;
        }
    }
}
