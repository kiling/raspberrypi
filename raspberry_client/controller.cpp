#include "controller.h"
#include <QDebug>
#include <wiringPi.h>
#include <wiringSerial.h>

Controller::Controller(QObject *parent)
	: QObject(parent)
{
	m_arduino = serialOpen("/dev/ttyUSB0", 115200);
	if (m_arduino < 0)
	{
		qDebug() << "Error serial open";
	}

	m_server = new QTcpServer(this);
	QObject::connect(m_server, SIGNAL(newConnection()), this, SLOT(newConnection()));

	if (!m_server->listen(QHostAddress::Any, 1273))
		qDebug() << "Server could not start!";
	else
		qDebug() << "Server started!";

	m_client = nullptr;
}

void Controller::newConnection()
{
	auto socket = m_server->nextPendingConnection();
	auto adress = socket->peerAddress().toString();
	auto port = QString::number(socket->peerPort());

	if (!m_client)
	{
		m_client = new PiSocket(socket, this);

		QObject::connect(m_client, SIGNAL(disconnected()), this, SLOT(onClientDisconnect()));
		QObject::connect(m_client, SIGNAL(commandReceived(Command&)), this, SLOT(commandReceived(Command&)));
		m_client->sendCommand("Greeting", "Hello client! I'm raspberry pi!");

		qDebug() << "New connection - " << adress << " " << port;
	}
	else
	{
		auto client = new PiSocket(socket, this);
		client->sendCommand("Error", "Only one connection!");
		qDebug() << "Only one connection!";
	}
}

void Controller::commandReceived(Command &command)
{
	qDebug() << command.first << " " << command.second;

	//int value = command.second.toInt();

	//serialPutchar(m_arduino, command.first[0]);
	//serialPutchar(m_arduino, value);
}

void Controller::onClientDisconnect()
{
	m_client = nullptr;
	qDebug() << "Client disconnected";
}
