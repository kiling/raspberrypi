#include "pisocket.h"
#include <QDataStream>
#include <QDebug>

PiSocket::PiSocket(QObject *parent)
	: QObject(parent)
{
	m_isServerSocket = false;
	m_length = 0;
	m_socet = new QTcpSocket(this);
	createSocket();
}

PiSocket::PiSocket(QTcpSocket *socet, QObject *parent)
	: QObject(parent)
{
	m_isServerSocket = true;

	m_length = 0;
	m_socet = socet;

	createSocket();
}

PiSocket::~PiSocket()
{
	m_isServerSocket = true; // Disable re connect
	delete m_socet;
}

bool PiSocket::connectToServer(QString ip, int port)
{
	m_ip = ip;
	m_port = port;

	qDebug() << "Try connect..";
	m_socet->connectToHost(ip, port);
	return m_socet->waitForConnected(1000);
}

void PiSocket::createSocket()
{
	QObject::connect(m_socet, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	QObject::connect(m_socet, SIGNAL(connected()), this, SLOT(onConnected()));
	QObject::connect(m_socet, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

void PiSocket::onReadyRead()
{
	QByteArray data = m_socet->readAll();

	if (m_length == 0)
	{
		QDataStream stream(&data, QIODevice::ReadOnly);

		stream >> m_length;

		size_t count = data.size() - sizeof(m_length);

		QByteArray buffer;

		buffer.resize(count);
		stream.readRawData(buffer.data(), count);

		m_data.clear();
		m_data.append(buffer);

		m_length -= buffer.size();
	}
	else
	{
		m_data.append(data);
		m_length -= data.size();
	}

	if (m_length < 0)
		m_length = 0;

	if (m_length == 0)
	{
		Command command = PiSocket::toCommand(m_data);
		emit commandReceived(command);
	}
}

void PiSocket::onConnected()
{
	if (!m_isServerSocket)
		qDebug() << "Connected\t";
}

void PiSocket::onDisconnected()
{
	emit disconnected();
	if (!m_isServerSocket)
	{
		qDebug() << "Disconnected\t";
		
		m_length = 0;
	}
}

void PiSocket::sendCommand(Command &command)
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << command;

	sendData(data);
}

void PiSocket::sendCommand(QString name, QString text)
{
	QVariant var(text);
	Command command(name, var);
	sendCommand(command);
}

void PiSocket::sendCommand(QString name, int value)
{
	QVariant var(value);
	Command command(name, var);
	sendCommand(command);
}

Command PiSocket::toCommand(QByteArray &data)
{
	Command cmd;
	QDataStream stream(&data, QIODevice::ReadOnly);
	stream >> cmd;
	return cmd;
}

void PiSocket::sendData(QByteArray &data)
{
	QByteArray allData;

	QDataStream stream(&allData, QIODevice::WriteOnly);
	stream << data;

	m_socet->write(allData);
}