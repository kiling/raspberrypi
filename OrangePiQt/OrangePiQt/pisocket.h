#ifndef PISOCKET_H
#define PISOCKET_H

#include <QObject>
#include <QtNetwork/QTcpSocket>

typedef QPair<QString, QVariant> Command;

class PiSocket : public QObject
{
	Q_OBJECT
public :
    explicit PiSocket(QObject *parent = 0); // Клиенский сокет
	explicit PiSocket(QTcpSocket *socet, QObject *parent = 0);// Серверный сокет
	virtual ~PiSocket();
	bool connectToServer(QString ip, int port);
	void sendCommand(Command &command);
	void sendCommand(QString name, QString text);
	void sendCommand(QString name, int value);
	QString &name();
signals:
	void commandReceived(Command &command);//Событие получения комманды
	void disconnected();
	private slots :
	    void onReadyRead();
	void onConnected();
	void onDisconnected();
	void createSocket();
private:
	void asynConnect();
	void sendData(QByteArray &data);
	static Command toCommand(QByteArray &data);
	int m_length;
	QByteArray m_data;
	QString m_ip;
	int m_port;
	bool m_isServerSocket;
	QTcpSocket *m_socet;
};

#endif // PISOCKET_H
