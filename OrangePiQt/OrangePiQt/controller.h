#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QtNetwork/QTcpServer>
#include "pisocket.h"

class Controller : public QObject
{
	Q_OBJECT
	public :
	    explicit Controller(QObject *parent = 0);
	public slots :
	    void newConnection();
	void commandReceived(Command &command);
	void onClientDisconnect();
private:
	PiSocket *m_client;
	QTcpServer *m_server;
	int m_arduino = -1;
};

#endif // CONTROLLER_H