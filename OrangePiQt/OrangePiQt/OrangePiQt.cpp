#include "controller.h"
#include <QDebug>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	qDebug() << "Rasbperry controller";
	Controller controller;
	return a.exec();
}