#include <Servo.h>

Servo servoX;
Servo servoY;

const int AIA_PWM  = 10;//PWM
const int AIB_DIR = 9;
const int BIA_PWM  = 12;//PWM
const int BIB_DIR = 11;
int PWM_FAST = 200;

void setup()
{
  Serial.begin(115200);
  Serial.println("Ok");
  servoX.attach(2);
  servoY.attach(3);

  pinMode(AIA_PWM, OUTPUT);
  pinMode(AIB_DIR, OUTPUT);
  pinMode(BIA_PWM, OUTPUT);
  pinMode(BIB_DIR, OUTPUT);
  softStop();
}

void runCommand(char command, int value = -1)
{
  switch (command)
  {
    case 'x':
      servoX.write(value);
      break;
    case 'y':
      servoY.write(value);
      break;
    case 's':
      PWM_FAST = 0;
      softStop();
      break;
    case 'f':
      PWM_FAST = 255;
      forward();
      break;
    case 'b':
      PWM_FAST = 255;
      backward();
      break;
    case 'r':
      PWM_FAST = 255;
      right();
      break;
    case 'l':
      PWM_FAST = 255;
      left();
      break;
  }
}

char command = ' ';

void loop() {
  if (Serial.available() > 0)
  {
    if (command != ' ')
    {
      int value = Serial.read();
      Serial.println(value);
      runCommand(command, value);
      command = ' ';
    }
    else
    {
      command = Serial.read();
      Serial.println(command);
      
      if (command != 'x' && command != 'y')
      {
        runCommand(command);
        command = ' ';
      }
    }
  }
}

void right()
{
  digitalWrite(AIB_DIR, HIGH );
  digitalWrite(BIB_DIR, LOW ); // direction = reverse

  analogWrite(AIA_PWM, 255 - PWM_FAST );
  analogWrite(BIA_PWM, PWM_FAST ); // PWM speed = fast
}

void left()
{
  digitalWrite(AIB_DIR, LOW ); // direction = reverse
  digitalWrite(BIB_DIR, HIGH );

  analogWrite(AIA_PWM, PWM_FAST ); // PWM speed = fast
  analogWrite(BIA_PWM, 255 - PWM_FAST );
}

void forward()
{
  digitalWrite( AIB_DIR, LOW ); // direction = reverse
  digitalWrite( BIB_DIR, LOW ); // direction = reverse

  analogWrite( AIA_PWM, PWM_FAST ); // PWM speed = fast
  analogWrite( BIA_PWM, PWM_FAST ); // PWM speed = fast
}

void backward()
{
  digitalWrite(AIB_DIR, HIGH );
  digitalWrite(BIB_DIR, HIGH );

  analogWrite(AIA_PWM, 255 - PWM_FAST );
  analogWrite(BIA_PWM, 255 - PWM_FAST );
}


void softStop()
{
  digitalWrite( AIB_DIR, LOW );
  digitalWrite( AIA_PWM, LOW );
  digitalWrite( BIB_DIR, LOW );
  digitalWrite( BIA_PWM, LOW );
}
